(in-package :dltx)
;;;
;;; char.lisp
;;;

#|
Simplest special char solution for parsing/rendering:

- parse and try identify meaningful structures which should be retained
  examples:
  - tex command:  \texcom[things]{stuff}
  - tex special char: \square \Box 
  - math stuff: $linesout[$tmp] =~ s/\^/\$\\mathchar"1356\$/g;
- at rendering time, escape anything not parsed in above manner

|#
(defun render-char (char stream)
  (cond ((eq char #\\)
	 (write-string "\\textbackslash{}" stream))
	((eq char #\#)
	 (write-string "\\#" stream))
	((eq char #\&)
	 (write-string "\\&" stream))
	((eq char #\$)
	 (write-string "\\$" stream)) 
	((eq char #\%)
	 (write-string "\\%" stream)) 
	((eq char #\_)
	 (write-string "\\_" stream)) 
	((eq char #\{)
	 (write-string "\\{" stream) 	; \\lbrace requires math mode
	 )
	((eq char #\})
	 (write-string "\\}" stream)	; \\rbrace requires math mode
	 )
	((eq char #\^)
	 ;;(write-string "\\mathchar\"1356 " stream)
	 (write-string "\\textasciicircum " stream) ; xunicode
	 )
	((eq char #\<)
	 (write-string "\\textless " stream))
	((eq char #\>)
	 (write-string "\\textgreater " stream))
	((eq char #\~)
	 ;; anticipate XeTeX: TTF or OTF fonts should use \char`~
	 (write-string "\\char`~ " stream))
	;; not a LaTeX special char of interest
	(t (write-char char stream))))

(defun lit-char (char stream)
  (render-char char stream))

(defun lit-chars (chars stream)
  "CHARS is a sequence of characters."
  (let ((chars-length (length chars)))
      (do ((x 0 (1+ x)))
	  ((= x chars-length))
	(lit-char (elt chars x) stream))))
