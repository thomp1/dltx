(defsystem dltx
  :serial t
  :description "Generate LaTeX markup."
  :components ((:file "packages")
	       (:file "util")
	       (:file "char")
	       (:file "latex")))
