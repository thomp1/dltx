(in-package :dltx)
;;;
;;; LaTeX output utils
;;;

;; \begin{foo} ... \end{foo} -- see the `env` function

(defun comment (some-string &key stream suppress-newline-p)
  (write-string "% " stream)
  (write-string some-string stream) 
  (unless suppress-newline-p
    (write-char #\Newline stream)))

(defun document (f &key opts-string stream)
  (l "begin" "document"
     :optional-arg (if opts-string
		       (list opts-string)
		       nil)
     :s stream)
  (funcall f stream)
  (l "end" "document" :s stream))

(defun documentclass (class &key opts-string stream)
  (l "documentclass" class :optional-arg (list opts-string) :s stream))

;; wrapper to provide flexibility in leveraging CLOS
(defun l (label args &key optional-arg opts-position s (suppress-newline-p t))
  "Construct a LaTeX command, including arguments and optional argument as specified. ARGS can be a single string (for commands which accept only a single argument) or a list of strings (for LaTeX commands which accept multiple arguments -- i.e., \somecommand{a}{b}...{n}). Any other value will be disregarded. An optional argument (for the LaTeX command), specified by OPTIONAL-ARG, can be specified either as a single string or as a list of strings (which will be expanded to a comma-separated series -- i.e., \somecommand[a,b,c,...n]). Any other value for OPTIONAL-ARG will be disregarded. If S is a stream, write to S. If S is NIL, return a string. If OPTS-POSITION is :AFTER, the [ ...options... ] component is written before the { ... } component when a { ... } component is present."
  (declare (string label))
  (l* label args optional-arg s
      :optional-arg-position opts-position
      :suppress-newline-p suppress-newline-p))

(defmethod l* (label args optional-arg (s null) &key optional-arg-position (suppress-newline-p t))
  "See docstring for L."
  (declare (string label))
  (with-output-to-string (string-stream)
    (l* label args optional-arg string-stream
	:suppress-newline-p suppress-newline-p
	:optional-arg-position optional-arg-position)))

;; lazy method for args as a string
(defmethod l* (label (args string) optional-arg (s stream) &key optional-arg-position (suppress-newline-p t))
  (declare (stream s)
	   (string label))
  (l* label (list args) optional-arg s :suppress-newline-p suppress-newline-p
      :optional-arg-position optional-arg-position))

(defmethod l* (label (args list) optional-arg (s stream) &key optional-arg-position (suppress-newline-p t))
  (declare (stream s)
	   (string label))
  (write-string "\\" s)
  (write-string label s)
  (when (and optional-arg (not (eq optional-arg-position :after)))
    (write-opts optional-arg s))
  (dolist (arg args)
    (write-char #\{ s)
    (write-string arg s)
    (write-char #\} s))
  (when (and (eq optional-arg-position :after) optional-arg)
    (write-opts optional-arg s))
  (unless suppress-newline-p
    (write-char #\Newline s)))

(defun env (label some-string &key opts-string stream)
  "begin/end latex environment"
  (declare (string label))
  (let ((return-string
	 (with-output-to-string (s)
	   (write-string "\\begin{" s)
	   (write-string label s)
	   (write-char #\} s)
	   (when opts-string
	       (write-char #\{ s)
	       (write-string opts-string s)
	       (write-char #\} s))
	   (write-char #\Newline s)
	   (if some-string
	       (write-string some-string s))
	   (write-string "
\\end{" s)
	   (write-string label s)
	   (write-string "}
" s))))
    (if stream (write-string return-string stream) return-string)))

(defun math (some-string &key stream)
  (write-char #\$ stream)
  (write-string some-string stream)
  (write-char #\$ stream))

(defun figure (id path &key caption stream title)
  "If specified, TITLE is a string. If specified, CAPTION is a string."
  (declare (stream stream)
	   (string path))
  (env "figure"
       (with-output-to-string (fig-s)
	 (env "center"
	      (includegraphics path)
	      :stream fig-s)
	 (when (or caption title)
	   (l "caption" caption :optional-arg (list title) :s fig-s))
	 (if id
	     (l "label" id :s fig-s)))
       :stream stream))

;; behaviors which might be implemented, if desirable
;; If IF-DOES-NOT-EXIST is :ERROR, then signal an error.
;; If IF-DOES-NOT-EXIST is :WARN, then warn.
(defun includegraphics (path &key (if-does-not-exist :substitute))
  "If IF-DOES-NOT-EXIST is :SUBSTITUTE, then substitute with markup indicating that the file does not exist. For any other value of IF-DOES-NOT-EXIST"
  (declare (string path))
  (let ((label "includegraphics")
	(content path))
    (cond ((eq if-does-not-exist :substitute)
	   ;; a typical invocation of \includegraphics specifies file name but does not specify type
	   (unless (directory (merge-pathnames (make-pathname :type :wild) (pathname path)))
	     (setf label nil)
	     (setf content (format nil "includegraphics: Unable to locate file ~A" path)))))
    (if label
	(l label content)
	content)))

(defun usepackage (packagename &key opts stream)
  (l "usepackage" packagename
     :optional-arg opts
     :s stream))

(defgeneric write-opts (opts s)
  (:documentation "Writes [ ... ] to stream S. OPTS is either a string or a list."))

(defmethod write-opts (opts (s stream))
  (write-char #\[ s)
  (write-opts-string opts s)
  (write-char #\] s))

(defmethod write-opts-string ((opts string) s)
  (write-opts-string-component opts s))

(defmethod write-opts-string ((opts list) s)
  (when opts
    (write-opts-string-component (string-list-to-string opts ",") s)))

;; currently just escapes ] character (if opts-string has an unescaped ], escape it)
(defun write-opts-string-component (opts-string s)
  "OPTS-STRING is a string. S is a stream."
  (let ((prev-char nil))
    (loop for char across opts-string
       do (progn
	    (when prev-char (write-char prev-char s))
	    (if (and (char= char #\])
		     (not (eq prev-char #\\)))
		(write-char #\\ s))
	    (setf prev-char char))
       finally (when char (write-char char s)))))

;; FIXME: move to util and use where appropriate
(defmacro with-enclosing (start end stream &body body)
  `(progn (write-string ,start ,stream)
	  ,@body
	  (write-string ,end ,stream)))
