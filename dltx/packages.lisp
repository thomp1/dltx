(in-package :cl-user)

(defpackage :dltx
  (:use :cl)
  (:documentation "The program is released under the terms of the Lisp Lesser GNU Public License http://opensource.franz.com/preamble.html, also known as the LLGPL. Copyright: David A. Thompson, 2014-2020")
  (:export document comment documentclass env figure includegraphics math usepackage 
	   l
	   lit-chars
	   with-enclosing))
