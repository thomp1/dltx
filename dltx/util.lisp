(in-package :dltx)
;;;
;;; utility functions
;;;

;; DCU/shared-string.lisp
(defun concs (&rest list) ; concs-fast2? 
  (declare (optimize (speed 3) (safety 0))
	   (list list))
  (let ((length (reduce #'+ list :key #'(lambda (x) (if (stringp x) (length x) 0)))))
    (declare (fixnum length))
    (let ((result (make-string length))
	  (start 0)
	  (end 0))
      (declare (fixnum start end))
      ;;(format t "length: ~A~%" length)
      (dolist (string? list)
	(when (stringp string?) 
	  (setf end (+ start (length string?)))
	  (replace result string? :start1 start :end1 end)
	  (setf start end)))
      result)))

;; DCU/shared-string.lisp
(defun merge-list-of-strings (some-list &optional (spacer ""))
  "Given a list of strings SOME-LIST, return a string containing each member of some-list, in the same order as in some-list where each string is separated by SPACER. 

If an object in the list isn't a string, use format nil with ~A to attempt a representation. If SPACER is a string, it will be used as a 'spacer' between arguments [a spacer is also added after the last list member]. (note: LIST-TO-STRING has format ~A functionality -> should we tighten this further consistent with name?)

Note(s): 
- CONCS-LIST is concs which handles list arg
- LIST-TO-STRING doesn't put spacer after last argument and can handle list with any members.
- Timing for this function:

   Evaluation took:
  1.909 seconds of real time
  1.548097 seconds of user run time
  0.068004 seconds of system run time
  13 page faults and
  32,203,680 bytes consed.

  versus recursive version

Evaluation took:
  1.539 seconds of real time
  1.424089 seconds of user run time
  0.084005 seconds of system run time
  1 page fault and
  32,225,520 bytes consed."
  (let ((some-string ""))
    (dolist (str some-list)
      (if (stringp str)
	  (setf some-string (concatenate 'string some-string str spacer))
	  ;; not a string so coerce it
	  (setf some-string (concatenate 'string
					 some-string
					 (format nil "~A" str)
					 spacer))))
    some-string))

;; DCU/shared-string.lisp
(defun string-list-to-string (some-list &optional (spacer " "))
  "SOME-LIST is a list of strings. Return a string."
  (if (consp some-list)
      (let ((lastitem (car (last some-list)))
	    (len (length some-list)))
	(concs
	 (merge-list-of-strings 
	  (subseq some-list 0 (- len 1))
	  spacer)
	 lastitem))
      ""))

